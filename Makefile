.phony:all clean

CFLAGS= -g -I./ -Wall

KERNEL_SRC=kernel/cothread.c kernel/lock.c kernel/mq.c
LIB_SRC=lib/coshell.c lib/command.c lib/colog.c lib/misc.c lib/timer.c

all:
	gcc $(CFLAGS) -o cothread_demo cothread_demo.c $(KERNEL_SRC) $(LIB_SRC)
	gcc $(CFLAGS) -o cothread_demo_rtos cothread_demo_rtos.c -pthread $(KERNEL_SRC) $(LIB_SRC)
	
clean:
	rm -rf cothread_demo
	

# cothread2

#### 介绍
cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。  
cothread2 将 cothread 调度器封装成组件，同一个系统中允许申请多个调度器。  
cothread2 和 RTOS 结合，同一个 RTOS 线程中可以开启多个协程，所有协程共享 RTOS 线程堆栈，降低多线程的 RAM 开销。  

#### 特点
和 RTOS 相比，cothread 有如下特点：  
轻量级、RAM利用率高、纯C、移植方便、调试方便。

#### 软件架构
一、调度器核心基于 C 语言的 switch-case 控制语句，通过源码行号记录每次执行的进度。

二、支持中断调用 thread_signal、thread_create，可以在中断建立处理线程。  

三、支持消息队列。  

四、支持软件定时器。  

五、在调度器上提供了几个基础线程：  
	1. shell 提供用户交互；  
	2. log 提供日志输出。  
  
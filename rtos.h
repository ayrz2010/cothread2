/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef RTOS_H_
#define RTOS_H_

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>

#define OS_THREAD_NR (100)
/* tick 1ms */
#define OS_HZ (1000)
#define OS_SCHED_HOOK_NR (20)
#define OS_IDLE_HOOK_NR (10)

#define THREAD_PRIO_NR (3)
#define THREAD_PRIO_HIGH 0
#define THREAD_PRIO_MID 1
#define THREAD_PRIO_LOW (THREAD_PRIO_NR - 1)

#define SHELL_BUFFER_SIZE (128)
#define LOG_BUFFER_SIZE (1024)

#include "kernel/cothread.h"
#include "kernel/lock.h"
#include "kernel/mq.h"
#include "lib/coshell.h"
#include "lib/command.h"
#include "lib/colog.h"
#include "lib/misc.h"
#include "lib/timer.h"

#define os_init cothread_init
#define os_start cothread_start
#define os_show_stat cothread_show_stat
#define thread_create cothread_create
#define thread_create_on_scheduler cothread_create_on_scheduler
#define thread_signal cothread_signal
#define os_enter_critical cothread_enter_critical
#define os_exit_critical cothread_exit_critical

#define OsEvent COEVENT
#define OsEventClr COEVENT_CLR

#define LOG colog_normal
#define LOG_WARN colog_normal
#define LOG_ERR colog_normal
#define LOG_POLL colog_poll

/* 这几个函数由硬件驱动提供 */
void system_timer_start(void);
void enable_interrupt(void);
void disable_interrupt(void);

#endif // RTOS_H_

